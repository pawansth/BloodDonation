var profile = {
    ajax   : null,
    result : null,
    ip     : null,
    SLLObj : null,
    cam    : null,
    init: function() {
        document.addEventListener('deviceready', function () {
            $('.div-prev-img').hide();
            profile.ip     = new ip();
            profile.ip     = profile.ip.remoteIpAddress();
            profile.ajax   = new ajax();
            profile.SLLObj = new SQLiteLogin();
            var userImg    = new userImage();
            profile.cam    = new profileImg();
            profile.showProfile();
            profile.eventListener();
            // account.eventListener();
        }, false);
    },
    eventListener: function() {
        $(document).on('click', '#post', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/post/post.html';
        });
        $(document).on('click', '#notification', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/notification/notification.html';
        });
        $(document).on('click', '.contact', function(e) {
            e.preventDefault();
            window.open('tel:'+$(this).text(), '_system')
        });
        $(document).on('click', '.profile-image', function() {
            $('.div-change-image-container').css({left: 0});
        });
        $(document).on('click', '.div-img-change-opt-1', profile.imgByCamera);
        $(document).on('click', '.div-img-change-opt-2', profile.imgByGallery);
        $(document).on('click', '.btn-done-upload', profile.updateProfileImage);
        $(document).on('click', '.btn-cancel-upload', function() {
            $('.div-prev-img').hide();
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="file:///android_asset/www/img/loading.gif">'+
                            '<h5 class="loadingText">Updating...</h5>'+
                        '</div>';
        $('.parent-container').prepend(templete);
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    showProfile: function() {
        var detail = profile.SLLObj.pullData();
        detail.done(function(informations) {
            var first_name  = '';
            var middle_name = '';
            var last_name   = '';
            var address     = '';
            var mobile      = '';
            var email       = '';
            var birthday    = '';
            var gander      = '';
            var blood_group = '';
            var image       = null;
            for (var idx in informations) {
                var information = informations[idx];
                image       = information.image;
                first_name  = information.first_name;
                middle_name = information.middle_name;
                last_name   = information.last_name;
                address     = information.address;
                mobile      = information.mobile;
                email       = information.email;
                birthday    = information.birthday;
                gander      = information.gander;
                blood_group = information.blood_group;
            }
            var d    = new Date(parseInt(birthday));
            var date = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
            var temp = '<div class="first_name" style="height: 60px;border-top: 2px solid #B71C1C; border-bottom: 2px solid #B71C1C;font-size: 14px;font-weight: bold;padding-top: 10px;padding-bottom: 10px; padding-left: 20px;">First name:<br/>'+first_name+'</div>'+
                        '<div class="user-detail-div middle_name">Middle name:<br/>'+middle_name+'</div>'+
                        '<div class="user-detail-div last_name">Last name:<br/>'+last_name+'</div>'+
                        '<div class="user-detail-div address">Address:<br/>'+address+'</div>'+
                        '<div class="user-detail-div mobile">Mobile:<br/><span class="contact">'+mobile+'</span></div>'+
                        '<div class="user-detail-div email">Email:<br/>'+email+'</div>'+
                        '<div class="user-detail-div birthday">Birthday:<br/>'+date+'</div>'+
                        '<div class="user-detail-div gander">Gender:<br/>'+gander+'</div>'+
                        '<div class="user-detail-div blood_group">Blood group:<br/>'+blood_group+'</div>';
            $('.detail-container').append(temp);
        }).fail(function(error) {
            console.log(error);
        });
    },
    updateProfileImage: function() {
        profile.addLoading();
        var res = profile.SLLObj.pullData();
        res.done(function(informations) {
            var access_token = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                access_token = information.access_token;
            }
            if (access_token.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = 'file:///android_asset/www/views/login/index.html';
            }
            var value = {
                access_token: access_token,
                image: $('.img-prev').attr('data')
            };
            var url = profile.ip+'?r=post/post-handler';
            var result = profile.ajax.ajaxCall(url,value);
            result.then(function(data) {
                profile.removeLoading();
                console.log(data);
                if(data.status === 1) {
                    profile.toastAlert(data.title);
                    $('.div-prev-img').hide();
                } else {
                    profile.toastAlert(data.title);
                    $('.div-prev-img').hide();
                }
            }).catch(function(error) {
                profile.removeLoading();
                console.error(error);
                profile.toastAlert('failed');
                $('.div-prev-img').hide();
            }); 
        }).fail(function(error) {
            profile.removeLoading();
            console.error(error);
            profile.toastAlert('faild');
            $('.div-prev-img').hide();
        });
    },
    imgByCamera: function() {
        profile.cam.openDefaultCamera();
        $('.div-change-image-container').css({left: '-100%'});
    },
    imgByGallery: function() {
        // var result = ;
        profile.cam.openFilePicker(1);
        $('.div-change-image-container').css({left: '-100%'});
    },
    toastAlert: function(msg) {
        window.plugins.toast.showWithOptions({
            message: msg,
            duration: 'long', // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself. 
            position: "bottom",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0) 
        });
    }
}
profile.init();