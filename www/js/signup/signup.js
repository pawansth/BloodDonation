var signup = {
    ajax      : null,
    userStatus: null,
    cam       : null,
    msgShow   : 0,
    init: function() {
            $('.step2-container').hide();
            $('.step3-container').hide();
            $('.step4-container').hide();
            $(".step1").css("background-color", "#B71C1C");
        document.addEventListener('deviceready', function () {
            $('select').material_select();
            signup.ip         = new ip();
            signup.ip         = signup.ip.remoteIpAddress();
            signup.ajax       = new ajax();
            signup.cam        = new camera();
            signup.userStatus = 0;
            signup.eventListener();
        }, false);
    },
    eventListener: function() {
        $(document).on('click','#btn-step1', signup.validateStep1);
        $(document).on('click','#btn-step2', signup.validateStep2);
        $(document).on('click','#btn-step3', signup.validateStep3);
        $(document).on('click', '#btn-signup', signup.validateStep4);
        $(document).on('click', '#camera', function(e) {
            e.preventDefault();
            signup.cam.openDefaultCamera();
        });
        $(document).on('click','#gallary', function(e) {
            e.preventDefault();
            signup.cam.openFilePicker();
        });
        $(document).on('focusout', '#firstName', function() {
            if($('#firstName').val().trim() === '') {
                $('.firstNameError').text('* First name field can not be empty');
                $('.firstNameError').css('height','20px');
            } else {
                $('.firstNameError').text('');
                $('.firstNameError').css('height','0px');
            }
        });
        $(document).on('focusout', '#lastName', function() {
            if($('#lastName').val().trim() === '') {
                $('.lastNameError').text('* Last name field can not be empty');
                $('.lastNameError').css('height','20px');
            } else {
                $('.lastNameError').text('');
                $('.lastNameError').css('height','0px');
            }
        });
        $(document).on('focusout', '#address', function() {
            if($('#address').val().trim() === '') {
                $('.addressError').text('* Address field can not be empty');
                $('.addressError').css('height','20px');
            } else {
                $('.addressError').text('');
                $('.addressError').css('height','0px');
            }
        });
        $(document).on('focusout', '#phone', function() {
            if($('#phone').val().trim() === '') {
                $('.phoneError').text('* Phone field can not be empty');
                $('.phoneError').css('height','20px');
            } else if(!($('#phone').val().length === 10)) {
                $('.phoneError').text('* Please enter 10 digit number');
                $('.phoneError').css('height','20px');
            } else {
                $('.phoneError').text('');
                $('.phoneError').css('height','0px');
            }
        });
        $(document).on('focusout', '#email', function() {
            if(!($('#email').val().trim() === '')) {
                var validationStatus = signup.isEmail();
                if(validationStatus === 0) {
                    $('.emailError').text('* Please enter valid email address');
                    $('.emailError').css('height','20px');
                } else {
                    $('.emailError').text('');
                    $('.emailError').css('height','0px');
                }
            }
        });
        $(document).on('focusout', '#dob', function() {
            if($('#dob').val().trim() === '') {
                $('.dobError').text('* Please enter your date of birth');
                $('.dobError').css('height','20px');
            } else {
                $('.dobError').text('');
                $('.dobError').css('height','0px');
            }
        });
        $('#bloodGroup').on('change', function() {
            $('.bloodGroupError').text('');
        });
        $(document).on('click', '#dob', signup.showDatePicker);
        $(document).on('focusout', '#userName', function() {
            if($('#userName').val().trim() === '') {
                $('.userNameError').text('* User name field can not be empty');
                $('.userNameError').css('height','20px');
            } else {
                var value = {
                    user_name: $('#userName').val()
                };
                var url = signup.ip + '?r=signup-handler/user-name-checker';
                var result = signup.ajax.ajaxCall(url,value);
                result.then(function(data) {
                    if(data.status === 0) {
                        signup.msgShow = 1;
                        $('.userNameError').text(data.msg);
                        $('.userNameError').css('height','20px');
                    } else {
                        signup.userStatus = 1;
                        $('.userNameError').text('');
                        $('.userNameError').css('height','0px');
                    }
                }).catch(function(error) {
                    console.log('* user name check error',error);
                    $('.userNameError').text('* Try another user name');
                });
            }
        });
        $(document).on('focusout', '#password', function() {
            if($('#password').val().trim() === '') {
                $('.passwordError').text('* Password field can not be empty');
                $('.passwordError').css('height','20px');
            } else {
                $('.passwordError').text('');
                $('.passwordError').css('height','0px');
            }
        });
        $(document).on('focusout', '#confirmPassword', function() {
            if($('#confirmPassword').val().trim() === '') {
                $('.confirmPasswordError').text('* Confirm password field can not be empty');
                $('.confirmPasswordError').css('height','20px');
            } else if(!($('#confirmPassword').val().trim() === $('#password').val().trim())) {
                $('.confirmPasswordError').text('* Password do not match');
                $('.confirmPasswordError').css('height','20px');
            } else {
                $('.confirmPasswordError').text('');
                $('.confirmPasswordError').css('height','0px');
            }
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="file:///android_asset/www/img/loading.gif">'+
                            '<h5 class="loadingText">Signup.....</h5>'+
                        '</div>';
        $('.parent-container').prepend(templete);
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    validateStep1: function() {
        if($('#firstName').val().trim() === '') {
            $('.firstNameError').text('* First name field can not be empty');
            return;
        } else if($('#lastName').val().trim() === '') {
            $('.lastNameError').text('* Last name field can not be empty');
            return;
        } else if($('#address').val().trim() === '') {
            $('.addressError').text('* Address field can not be empty');
            return;
        } else if($('#phone').val().trim() === '') {
            $('.phoneError').text('* Mobile number field can not be empty');
            return;
        } else if(!($('#phone').val().length === 10)) {
            $('.phoneError').text('* Please enter 10 digit number');
            return;
        } else {
            $('.step1-container').hide();
            setTimeout(function() {
                $('.step2-container').show();
                $(".step2").css("background-color", "#B71C1C");
                var d = new Date();
                var datestring = (d.getFullYear()-20) + "-" + (d.getMonth()+1) + "-"+ d.getDate();
                $('#dob').val(datestring);
            }, 30);
        }
    },
    validateStep2: function() {
        if(!($('#email').val().trim() === '')) {
            var validationStatus = signup.isEmail();
            if(! (validationStatus === 1)) {
                $('.emailError').text('* Please enter valid email address');
                return;
            }
        } else if($('#dob').val().trim() === '') {
            $('.dobError').text('* Please enter your date of birth');
            $('.dobError').css('height','20px');
            return;
        } else if ($('#bloodGroup').val() === null) {
            $('.bloodGroupError').text('* select your blood group');
            return
        }
        $('.step2-container').hide();
        setTimeout(function() {
            $('.step3-container').show();
        $(".step3").css("background-color", "#B71C1C");
        },30);
    },
    isEmail: function() {
    var email = $('#email').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
  showDatePicker: function() {
      console.log(new Date());
      var d = new Date();
      var oldYer = d.getFullYear();
      var newYear = d.getFullYear() - 20;
      d = d.toString();
      d = d.replace(oldYer, newYear);
      var options = {
          date: new Date(d),
          mode: 'date',
          maxDate: Date.parse(new Date()),
          windowTitle: 'Set your birthday'
        };

        datePicker.show(options, function(d){
          var datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
          $('#dob').val(datestring);
        });
    },
  validateStep3: function() {
      if($('#userName').val().trim() === '') {
          $('.userNameError').text('* User name field can not be empty');
          return;
      } else if(signup.userStatus === 0) {
          if(!(signup.msgShow === 1)) {
              $('.userNameError').text('* We are checking your user name');
                return;
          } else {
              return;
          }
      } else if($('#password').val().trim() === '') {
          $('.passwordError').text('* Password field can not be empty');
          return;
      } else if($('#confirmPassword').val().trim() === '') {
          $('.confirmPasswordError').text('* Confirm password field can not be empty');
          return;
      } else if(!($('#confirmPassword').val().trim() === $('#password').val().trim())) {
          $('.confirmPasswordError').text('* Password do not match');
          return;
      } else {
          $('.step3-container').hide();
          setTimeout(function() {
              $('.step4-container').show();
          $(".step4").css("background-color", "#B71C1C");
          },30);
      }
  },
  validateStep4: function() {
      if($('#userImg').attr('test') === '') {
          $('.userImgError').text('* User image is required');
          $('.userImgError').css('margin-bottom','5px');
          return;
      }
      signup.addLoading();
      var imageData = $('#userImg').attr('src');
      var value     = {
          first_name : $('#firstName').val(),
          middle_name: $('#middleName').val(),
          last_name  : $('#lastName').val(),
          address    : $('#address').val(),
          mobile     : $('#phone').val(),
          email      : $('#email').val(),
          birthday   : new Date($('#dob').val()).getTime(),
          gander     : $('input[name=gander]:checked').val(),
          blood_group: $('#bloodGroup').val(),
          user_name  : $('#userName').val(),
          password   : $('#password').val(),
          imageData  : imageData
      };
      console.log(value);
      /* First uploading image to server using
      file transfer protocal*/
                var url = signup.ip+'?r=signup-handler/signup-handler';
                var result = signup.ajax.ajaxCall(url,value);
                result.then(function(data) {
                    console.log(data);
                    if(data.status === 1) {
                        signup.toastAlert('success');
                        window.location.href = 'file:///android_asset/www/views/login/index.html';
                    }
                    if(data.status === 0) {
                        signup.removeLoading();
                        signup.toastAlert(data.msgBody);
                    }
                }).catch(function(error) {
                    console.log('user name check error',error);
                    signup.removeLoading();
                    navigator.notification.alert(
                        'Faild to signup. There may be proble in internet. Check your internet connection and try again.',
                        null,
                        'ERROR',
                        'OK'
                    );
                });
    //   var options            = new FileUploadOptions();
    //   options.fileKey        = "file";
    //   options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpeg';
    //   options.mimeType       = "image/jpeg";
    //   console.log(options.fileName);
    //   // Creating new object to stor field data
    //   var params             = new Object();
    //   params.value           = value;
    //   // stroing params object to options
    //   options.params         = params;
    //   options.chunkedMode    = false;

    //   var ft = new FileTransfer();
    //   ft.upload(imageData, signup.ip+'?r=signup-handler/signup-handler', function(result){
    //     var response = $.parseJSON(result.response);
    //     console.log(response);
    //     signup.removeLoading();
    //     if(response.status === 1) {
    //       signup.toastAlert('success');
    //         window.location.href = 'file:///android_asset/www/views/login/index.html';
    //     }
    //     if(response.status === 0) {
    //       navigator.notification.alert(
    //                 response.msgBody,
    //                 null,
    //                 response.msgTitle,
    //                 'OK'
    //             );
    //     }
    //   }, function(error){
    //     console.error(error);
    //     signup.removeLoading();
    //     navigator.notification.alert(
    //       'Faild to signup. There may be proble in internet. Check your internet connection and try again.',
    //       null,
    //       'ERROR',
    //       'OK'
    //     );
    //   }, options);
  },
    toastAlert: function(msg) {
        window.plugins.toast.showWithOptions({
            message: msg,
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself. 
            position: "bottom",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0) 
        });
    }
}
signup.init();