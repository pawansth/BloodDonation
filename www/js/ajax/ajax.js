var ajax = function() {
    var aj = {
        ajaxCall: function(urls, values) {
            return aj.transferCall(urls, values);    
        },
        transferCall: function(urls, values) {
            return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
        }
    }
    return aj;
}