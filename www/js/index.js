
var app = {
    // Application Constructor
    SLLObj: null,
    networkConnection: null,
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        // At the starting of apps first create table
      var STG = new SQLiteTableGenerator();
      // console.log('after obj create');
      var result = STG.initialize();
      result.done(function(resultReturn){
        console.log('table is successfully created');
        app.SLLObj = new SQLiteLogin();
        app.networkConnection = new networkInformation();
        app.networkConnection.checkConnection();
        console.log(app.networkConnection);
        $('.container').load('file:///android_asset/www/views/splashscreen.html');
        var splashscreenObj = new splashscreen();
      }
      ).fail(function(error){
        console.error(error);
      });
    //     this.receivedEvent('deviceready');
        // window.location.href = "file:///android_asset/www/views/login/index.html";
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();