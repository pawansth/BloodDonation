var userImage = function() {
    var image = {
        SLLObj : null,
        init: function() {
            image.SLLObj = new SQLiteLogin();
            var pullData = image.SLLObj.pullData();
            pullData.done(function(informations) {
                var image = null;
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    image = information.image;
                }
                $('.btn-user-account').attr('src','data:image/jpeg;base64,'+image);
            });
        }
    }
    return image.init();
}