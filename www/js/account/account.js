var account = {
    ajax     : null,
    result   : null,
    ip       : null,
    SLLObj   : null,
    SLDObj   : null,
    deviceMng: null,
    init: function() {
        document.addEventListener('deviceready', function () {
            account.ip     = new ip();
            account.ip     = account.ip.remoteIpAddress();
            account.ajax   = new ajax();
            account.SLLObj = new SQLiteLogin();
            account.SLDObj = new SQLiteDevice();
            var userImg    = new userImage();
            account.deviceMng = new deviceManager();
            account.eventListener();
        }, false);
    },
    eventListener: function() {
        $(document).on('click', '#post', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/post/post.html';
        });
        $(document).on('click', '#notification', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/notification/notification.html';
        });
        $(document).on('click', '.logout', account.logout);
        $(document).on('click', '.view-profile-container', function() {
            window.location.href = 'file:///android_asset/www/views/profile/profile.html';
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="file:///android_asset/www/img/loading.gif">'+
                            '<h5 class="loadingText">Log out.....</h5>'+
                        '</div>';
        $('.parent').prepend(templete);
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    logout: function() {
        account.addLoading();
        var result = account.SLDObj.pullData();
        result.done(function(informations) {
            var device_id = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                device_id = information.device_id;
            }
            console.log('device id: '+device_id);
            if(device_id.trim() === '') {
                console.log('device id is not set');
                account.databaseDelete();
            } else {
                var res = account.SLLObj.pullData();
                res.done(function(informations) {
                    var access_token = '';
                    for (var idx in informations) {
                        var information = informations[idx];
                        //alert(information.name);
                        access_token = information.access_token;
                    }
                    if (access_token.trim() === '') {
                        console.log('there is no accessToken');
                        window.location.href = 'file:///android_asset/www/views/login/index.html';
                    }

                    var value = {
                        access_token: access_token,
                        device_id   : device_id
                    };
                    var url = account.ip+'?r=device-id-handler/delete-device';
                    var result = account.ajax.ajaxCall(url,value);
                    result.then(function(data) {
                        console.log(data);
                        if(data.status === 1) {
                            account.databaseDelete();
                        } else {
                            account.removeLoading();
                            account.toastAlert('FAILED');
                        }
                    }).catch(function(error) {
                        account.removeLoading();
                        console.error(error);
                        account.toastAlert('FAILED');
                        // navigator.notification.alert(
                        //     'Faild to post your request.' +
                        //     ' Check out internet connection and try again',
                        //     null,
                        //     'Error',
                        //     'OK'
                        // );
                    });
                }).fail(function(error) {
                    account.removeLoading();
                    account.toastAlert('FAILED');
                    console.log(error);
                });
            }
        }).fail(function(error) {
            account.removeLoading();
            account.toastAlert('FAILED');
            console.log(error);
        });
    },
    databaseDelete: function() {
        var deviceIdDelete = account.SLDObj.deleteData();
        deviceIdDelete.done(function() {
            var logout = account.SLLObj.deleteData();
            logout.done(function(res) {
                console.log(res);
                setTimeout(function() {
                    window.location.href = 'file:///android_asset/www/views/login/index.html';
                }, 2000);
            }).fail(function(error) {
                account.removeLoading();
                console.error(error);
            });
        }).fail(function(error) {
            account.removeLoading();
            console.log(error);
        });
    },
    toastAlert: function(msg) {
        window.plugins.toast.showWithOptions({
            message: msg,
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself. 
            position: "bottom",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0) 
        });
    }
}
account.init();