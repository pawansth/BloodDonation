var splashscreen = function() {
    var progresHandler = {

        proValObj   : null,
        SQLLObj     : null,
        ip          : null,
        gloabalToken: null,
        ajax        : null,
        initialize: function() {
            this.proValObj = progressValue();
            this.ip = new ip();
            this.ip = this.ip.remoteIpAddress();
            this.SQLLObj = new SQLiteLogin();
            this.ajax = new ajax();
            this.initLogin();
        },

        initLogin: function() {
            var result = progresHandler.SQLLObj.pullData();
            result.done(function(informations) {
                var access_token = '';
                var status      = '';
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    access_token = information.access_token;
                    status       = information.status;
                }
                if (access_token.trim() === '') {
                  console.log('there is no accessToken');
                    $("progress").attr("value", 100);
                    window.location.href = 'file:///android_asset/www/views/login/index.html';
                    return;
                } else if(status === 0) {
                    progresHandler.SQLLObj.deleteData();
                    $("progress").attr("value", 100);
                    window.location.href = 'file:///android_asset/www/views/login/index.html';
                    return;
                }
                progresHandler.gloabalToken = access_token;

                var value = {
                    access_token: access_token
                };
                var url = progresHandler.ip+'?r=check-token/check';
                var result = progresHandler.ajax.ajaxCall(url,value);
                result.then(function(data) {
                    if(data.success) {
                        window.FirebasePlugin.onNotificationOpen(function(notification) {
                            window.location.href = 'file:///android_asset/www/views/notification/notification.html';
                        }, function(error) {
                            console.error(error);
                        });
                        window.location.href = 'file:///android_asset/www/views/post/post.html';
                    } else {
                            $("progress").attr("value", 100);
                            window.location.href = 'file:///android_asset/www/views/login/index.html';
                        }
                }).catch(function(error) {
                    console.error(error);
                    navigator.notification.alert(
                        'Unable to Connect with the Server.' +
                        ' Check out internet connection and try again',
                        progresHandler.initLogin,
                        'Connection error',
                        'Try again'
                    );
                    $("progress").attr("value", 0);
                }); 
            }).fail(function(error) {
                console.error(error);
            });
        }

    }

    return progresHandler.initialize();
}
