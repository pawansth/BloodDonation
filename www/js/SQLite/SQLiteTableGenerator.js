var SQLiteTableGenerator = function() {
    var service = {};
var db = new SQLiteConnection();

    service.initialize = function() {
      // console.log('inside table generator');
        var deferred = $.Deferred();
        db.transaction(function(tx) {
                tx.executeSql(
                        'CREATE TABLE IF NOT EXISTS user_detail ' +
                        '(id integer primary key AUTOINCREMENT, first_name varchar(50), middle_name varchar(50), '+
                        'last_name varchar(50), address varchar(50), mobile integer, email varchar(50), birthday integer, '+
                        'gander varchar(50), blood_group varchar(50), access_token varchar(100), image LONGBLOB, status integer)', [],
                        function(tx, res) {
                            tx.executeSql(
                                'CREATE TABLE IF NOT EXISTS device_token '+
                                '(id integer primary key AUTOINCREMENT, device_id varchar(255))',[],
                                function(tx, res) {
                                    deferred.resolve(service);
                                }, function(tx, res) {
                                    deferred.reject(res);
                                }
                            );
                        },
                        function(tx, res) {
                            deferred.reject(res);
                        });
        });
    return deferred.promise();
}
return service;
}
