var SQLiteDevice = function() {
    // getting sqlite connection
  var db = new SQLiteConnection();

  var service = {};

  service.pushData = function(device_id) {
      var deferred = $.Deferred();
      db.transaction(function(tx) {
        var query = "INSERT INTO device_token (device_id) VALUES (?)";
          tx.executeSql(query, [device_id], function(tx, res) {
              console.log('device id get by sqlit function'+device_id);
              deferred.resolve(res);
          }, function(e) {
              deferred.reject(e);
          });
      });
      return deferred.promise();
  },

  service.pullData = function() {
          //alert('inside pulldata function');
          var deferred = $.Deferred();
          db.transaction(function(tx) {
            var query = 'SELECT * FROM device_token';
              tx.executeSql(query, [], function(tx, res) {
                  var informations = [];
                  console.log('data length is '+res.rows.length);
                  for (var i = 0; i < res.rows.length; i++) {
                      var information = {
                          device_id  : res.rows.item(i).device_id
                      };

                      informations.push(information);
                  }
                  deferred.resolve(informations);

              }, function(e) {
                  deferred.reject(e);
              });
          });
          return deferred.promise();
      },

      service.deleteData = function() {
          var deferred = $.Deferred();
          db.transaction(function(tx) {
            var query = 'DELETE FROM device_token';
              tx.executeSql(query, [], function(tx, res) {
                  deferred.resolve();
              }, function(e) {
                  deferred.reject(e);
              });
          });
          return deferred.promise();
      }

  return service;
}