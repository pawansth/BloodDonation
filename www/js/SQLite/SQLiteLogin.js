var SQLiteLogin = function() {
  // getting sqlite connection
  var db = new SQLiteConnection();

  var service = {};

  service.pushData = function(access_token, data, status) {
      var deferred = $.Deferred();
      db.transaction(function(tx) {
        var query = 'INSERT INTO user_detail (first_name, middle_name, last_name, '+
                    'address, mobile, email, birthday, gander, blood_group, access_token, image, status) VALUES ('+
                    '?,?,?,?,?,?,?,?,?,?,?,?)';
          tx.executeSql(query, [data.first_name,data.middle_name,data.last_name,
                                data.address,data.mobile,data.email,data.birthday,
                                data.gander, data.blood_group, access_token, data.image, status], function(tx, res) {
              deferred.resolve(res);
          }, function(e) {
              deferred.reject(e);
          });
      });
      return deferred.promise();
  },

  service.pullData = function() {
          //alert('inside pulldata function');
          var deferred = $.Deferred();
          db.transaction(function(tx) {
            var query = 'SELECT * FROM user_detail';
              tx.executeSql(query, [], function(tx, res) {
                  var informations = [];
                  console.log('data length is '+res.rows.length);
                  for (var i = 0; i < res.rows.length; i++) {
                      var information = {
                          first_name  : res.rows.item(i).first_name,
                          middle_name : res.rows.item(i).middle_name,
                          last_name   : res.rows.item(i).last_name,
                          address     : res.rows.item(i).address,
                          mobile      : res.rows.item(i).mobile,
                          email       : res.rows.item(i).email,
                          birthday    : res.rows.item(i).birthday,
                          gander      : res.rows.item(i).gander,
                          blood_group : res.rows.item(i).blood_group,
                          access_token: res.rows.item(i).access_token,
                          image       : res.rows.item(i).image,
                          status      : res.rows.item(i).status
                      };

                      informations.push(information);
                  }
                  deferred.resolve(informations);

              }, function(e) {
                  deferred.reject(e);
              });
          });
          return deferred.promise();
      },

      service.deleteData = function() {
          var deferred = $.Deferred();
          db.transaction(function(tx) {
            var query = 'DELETE FROM user_detail';
              tx.executeSql(query, [], function(tx, res) {
                  deferred.resolve(res);
              }, function(e) {
                  deferred.reject(e);
              });
          });
          return deferred.promise();
      }

  return service;

}
