var post =  {
    ajax     : null,
    result   : null,
    ip       : null,
    SLLObj   : null,
    SLDObj   : null,
    bckBtn   : 0,
    bckHou   : null,
    bckMin   : null,
    bckSec   : null,
    msgLength: 0,
    deviceMng: null,
    init: function() {
        document.addEventListener('deviceready', function () {
            post.ip     = new ip();
            post.ip     = post.ip.remoteIpAddress();
            post.ajax   = new ajax();
            post.SLLObj = new SQLiteLogin();
            post.SLDObj = new SQLiteDevice();
            var userImg = new userImage();
            post.deviceMng = new deviceManager();
            document.addEventListener("backbutton", function (e) {
                e.preventDefault();
                if(post.bckBtn === 1) {
                    var date = new Date();
                    var hou = date.getHours();
                    var min = date.getMinutes();
                    var sec = date.getSeconds();
                    if(hou === post.bckHou && min === post.bckMin && ((sec-post.bckSec)<2)) {
                        navigator.app.exitApp();
                    } else {
                        post.bckHou = 0;
                        post.bckMin = 0;
                        post.bckSec = 0;
                        post.bckBtn = 0;
                    }
                } else {
                    var date = new Date();
                    post.bckHou = date.getHours();
                    post.bckMin = date.getMinutes();
                    post.bckSec = date.getSeconds();
                    post.bckBtn = 1;
                }
            }, false );
            post.eventListener();
        }, false);
    },
    eventListener: function() {
        $(document).on('click', '.btn-user-account', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/account/account.html';
        });
        $(document).on('input', '#msg', function() {
            if($('#msg').val().length < 501) {
                $('.msgError').text('');
                post.msgLength = 0;
            } else {
                $('.msgError').text('You can write only 500 character');
                post.msgLength = 1;
            }
        });
        $(document).on('focusout', '#msg', function() {
            if($('#msg').val().trim() === '') {
                $('.msgError').text('Message field can not be empty');
            } else if(post.msgLength === 1) {
                $('.msgError').text('You can write only 500 character');
            } else {
                $('.msgError').text('');
            }
        });
        $(document).on('focusout', '#contact_1', function() {
            if($('#contact_1').val().trim() === '') {
                $('.contact_1Error').text('Contact field can not be empty');
            } else if(!($('#contact_1').val().length === 10)) {
                $('.contact_1Error').text('Phone number should be 10 digit');
            } else {
                $('.contact_1Error').text('');
            }
        });
        $(document).on('focusout', '#contact_2', function() {
            if(!($('#contact_2').val().trim() === '')) {
                if(!($('#contact_2').val().length === 10)) {
                    $('.contact_2Error').text('Phone number should be 10 digit');
                } else {
                    $('.contact_2Error').text('');
                }
            } else {
                $('.contact_2Error').text('');
            }
        });
        $(document).on('click', '#notification', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/notification/notification.html';
        });
        $(document).on('click', '#btn-post', post.postHelp);
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="file:///android_asset/www/img/loading.gif">'+
                            '<h5 class="loadingText">Sending.....</h5>'+
                        '</div>';
        $('.parent').prepend(templete);
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    postHelp: function() {
        if(post.msgLength === 1) {
            return;
        } else if($('#msg').val().trim() === '') {
            $('.msgError').text('Message field can not be empty');
            return;
        } else if($('#contact_1').val().trim() === '') {
            $('.contact_1Error').text('Contact field can not be empty');
            return;
        } else if(!($('#contact_1').val().length === 10)) {
            $('.contact_1Error').text('Phone number should be 10 digit');
            return;
        } else if(!($('#contact_2').val().trim() === '')) {
            if(!($('#contact_2').val().length === 10)) {
                $('.contact_2Error').text('Phone number should be 10 digit');
                return;
            }
        }
        post.addLoading();
        var device = post.SLDObj.pullData();
        device.done(function(informations) {
            var device_id = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                device_id = information.device_id;
            }
            console.log('device id: '+device_id);
                var idScuccess = post.deviceMng.deviceIdManager();
                if(idScuccess === 0) {
                    post.postHelp();
                }
                var res = post.SLLObj.pullData();
                res.done(function(informations) {
                    var access_token = '';
                    for (var idx in informations) {
                        var information = informations[idx];
                        //alert(information.name);
                        access_token = information.access_token;
                    }
                    if (access_token.trim() === '') {
                        console.log('there is no accessToken');
                        window.location.href = 'file:///android_asset/www/views/login/index.html';
                    }
                    var value = {
                        access_token: access_token,
                        message     : $('#msg').val(),
                        blood_group : $('#bloodGroup').val(),
                        contact_1   : $('#contact_1').val(),
                        contact_2   : $('#contact_2').val(),
                        device_id   : device_id
                    };
                    var url = post.ip+'?r=post/post-handler';
                    var result = post.ajax.ajaxCall(url,value);
                    result.then(function(data) {
                        post.removeLoading();
                        console.log(data);
                        if(data.status === 1) {
                            $('#msg').val('');
                            $("#bloodGroup option[value='A+']").prop('selected', true);
                            $('#contact_1').val('');
                            $('#contact_2').val('');
                            post.toastAlert(data.title);
                        } else {
                            post.toastAlert(data.title);
                        }
                    }).catch(function(error) {
                        post.removeLoading();
                        console.error(error);
                        post.toastAlert('failed');
                        // navigator.notification.alert(
                        //     'Faild to post your request.' +
                        //     ' Check out internet connection and try again',
                        //     null,
                        //     'Error',
                        //     'OK'
                        // );
                    }); 
                }).fail(function(error) {
                    post.removeLoading();
                    console.error(error);
                    post.toastAlert('faild');
                });
        }).fail(function(error) {
            console.error(error);
            post.removeLoading();
            post.toastAlert('faild');
        });
    },
    toastAlert: function(msg) {
        window.plugins.toast.showWithOptions({
            message: msg,
            duration: "long", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself. 
            position: "bottom",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0) 
        });
    }
}
post.init();