var notification = {
    ajax   : null,
    result : null,
    ip     : null,
    SLLObj : null,
    last_id: null,
    count  : 0,
    fuLock : 0,
    init: function() {
        document.addEventListener('deviceready', function () {
            notification.ip     = new ip();
            notification.ip     = notification.ip.remoteIpAddress();
            notification.ajax   = new ajax();
            notification.SLLObj = new SQLiteLogin();
            var userImg = new userImage();
            notification.loadPost();
            notification.eventListener();
        }, false);
    },
    eventListener: function() {
        $(document).on('click', '.btn-user-account', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/account/account.html';
        });
        $(document).on('click', '#post', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/post/post.html';
        });
        $(document).on('click', '#notification', function(e) {
            e.preventDefault();
            notification.last_id = null;
            notification.count   = 0;
            notification.loadPost();
        });
        $(document).on('click', '.contact', function(e) {
            e.preventDefault();
            window.open('tel:'+$(this).text(), '_system')
        });
        $(window).on('scroll', function () {
            if(!(notification.count === 5)) {
                // alert('there is no more data');
                return;
            } else {
                if(notification.fuLock === 0){
                    // alert('there is more data');
                    notification.fuLock = 1;
                    notification.loadPost();
                }
            }
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="file:///android_asset/www/img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $('.parent').prepend(templete);
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    loadPost: function() {
        var res      = notification.SLLObj.pullData();
        var img_path = 'http://www.matribhoomi.org/blood-donation-api/web/';
        res.done(function(informations) {
            var access_token = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                access_token = information.access_token;
            }
            if (access_token.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = 'file:///android_asset/www/views/login/index.html';
            }
            if(notification.last_id === null) {
                notification.addLoading();
            }
            var value = {
                access_token: access_token,
                last_id     : notification.last_id
            };
            var url = notification.ip+'?r=post/post-pull';
            var result = notification.ajax.ajaxCall(url,value);
            result.then(function(data) {
                notification.removeLoading();
                console.log(data);
                if(data.status === 1) {
                    notification.count = data.postDatas.length;
                    var templete = '';
                    if(notification.last_id === null) {
                        $('.container-padding').empty();
                    }
                    for(var i = 0; i < notification.count; i++) {
                        var d    = new Date(parseInt(data.postDatas[i].date_time)*1000);
                        var date = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
                        templete = '<!--Div that contant data of notification-->'+
                                    '<div class="data-container">'+
                                        '<!--Div that contant real notification-->'+
                                        '<div class="inside-container">'+
                                            '<div class="notification-header">'+
                                                '<div class="notification-image">'+
                                                    '<img class="user-image" src="'+img_path+'/'+data.postDatas[i].image_url+'">'+
                                                '</div>'+
                                                '<div class="user-post-detail">'+
                                                    '<span id="user_name">'+data.postDatas[i].user_name+'</span><br/>'+
                                                    '<span id="date_time">'+date+'</span>'+
                                                '</div>'+
                                            '</div>'+
                                            '<!--Div to contant message-->'+
                                            '<div class="message-container">'+
                                                data.postDatas[i].message+
                                                '<br/>Require blood group:&nbsp;&nbsp;&nbsp;<span class="blood_group">'+data.postDatas[i].blood_group+'</span>'+
                                                '<br/>Mobile no:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="contact contact_1">'+data.postDatas[i].contact_1+'</span>'+
                                                ',&nbsp;&nbsp;<span class="contact contact_2">'+data.postDatas[i].contact_2+'</span>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                                    $('.container-padding').append(templete);
                        notification.last_id = data.postDatas[i].id;
                    }
                    notification.fuLock = 0;
                } else if(data.status === 2) {
                    window.location.href = 'file:///android_asset/www/views/login/index.html';
                }
            }).catch(function(error) {
                notification.removeLoading();
                console.error(error);
                notification.toastAlert('faild');
            });
        }).fail(function(error) {
            console.error(error);
            notification.removeLoading();
            notification.toastAlert('faild');
        });
    },
    toastAlert: function(msg) {
        window.plugins.toast.showWithOptions({
            message: msg,
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself. 
            position: "bottom",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0) 
        });
    }
}
notification.init();