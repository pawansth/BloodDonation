var deviceManager = function() {
    var device = {
        ajax     : null,
        result   : null,
        ip       : null,
        SLLObj   : null,
        SLDObj   : null,
        globalToken: '',
        globalDeviceId: '',
        init: function() {
            document.addEventListener('deviceready', function () {
                device.ip     = new ip();
                device.ip     = device.ip.remoteIpAddress();
                device.ajax   = new ajax();
                device.SLLObj = new SQLiteLogin();
                device.SLDObj = new SQLiteDevice();
            }, false);
        },
        deviceIdManager: function() {
            window.FirebasePlugin.getToken(function(token) {
                device.globalToken = token;
                // save this server-side and use it to push notifications to this device
                var devicData = device.SLDObj.pullData();
                devicData.done(function(informations) {
                    for (var idx in informations) {
                        var information = informations[idx];
                        //alert(information.name);
                        device.globalDeviceId = information.device_id;
                    }
                    if(device.globalDeviceId.trim() ==='') {
                        var res = device.SLLObj.pullData();
                        res.done(function(informations) {
                            var access_token = '';
                            for (var idx in informations) {
                                var information = informations[idx];
                                //alert(information.name);
                                access_token = information.access_token;
                            }
                            if (access_token.trim() === '') {
                                console.log('there is no accessToken');
                                window.location.href = 'file:///android_asset/www/views/login/index.html';
                            }
                            var value = {
                                access_token: access_token,
                                device_id   : device.globalToken
                            };
                            var url = device.ip+'?r=device-id-handler/new-device';
                            var result = device.ajax.ajaxCall(url,value);
                            result.then(function(data) {
                                console.log(data);
                                if(data.status === 1) {
                                    console.log('device id recorded to server');
                                    var result = device.SLDObj.pushData(data.token);
                                    result.done(function() {
                                        console.log('device id recorded to client');
                                        return 1;
                                    }).fail(function(error) {
                                        console.error(error);
                                        return 0;
                                    });
                                } else if(data.status === 2) {
                                    window.location.href = 'file:///android_asset/www/views/login/index.html';
                                }
                            }).catch(function(error) {
                                console.error(error);
                            });
                        }).fail(function(error) {
                            console.error(error);
                        });
                    } else {
                        if(!(device.globalDeviceId === device.globalToken)) {
                            console.log('device id is refreshed and changed');
                            var res = device.SLLObj.pullData();
                            res.done(function(informations) {
                                var access_token = '';
                                for (var idx in informations) {
                                    var information = informations[idx];
                                    //alert(information.name);
                                    access_token = information.access_token;
                                }
                                if (access_token.trim() === '') {
                                    console.log('there is no accessToken');
                                    window.location.href = 'file:///android_asset/www/views/login/index.html';
                                }
                                var value = {
                                    access_token  : access_token,
                                    new_device_id : device.globalToken,
                                    old_device_id : device.globalDeviceId
                                };
                                var url = device.ip+'?r=device-id-handler/update-device';
                                var result = device.ajax.ajaxCall(url,value);
                                result.then(function(data) {
                                    console.log(data);
                                    if(data.status === 1) {
                                        var deleteResult =device.SLDObj.deleteData(); 
                                        deleteResult.done(function() {
                                            var result = device.SLDObj.pushData(data.token);
                                            result.done(function() {
                                                return 1;
                                            }).fail(function(error) {
                                                console.error(error);
                                                return 0;
                                            });
                                        }).fail(function(error) {
                                            console.error(error);
                                            return 0;
                                        });
                                    }
                                }).catch(function(error) {
                                    console.error(error);
                                    return 0;
                                });
                            }).fail(function(error) {
                                console.error(error);
                                return 0;
                            });
                        } else {
                            return 1;
                        }
                    }
                }).fail(function(error) {
                    console.error(error);
                    return 0;
                });
            }, function(error) {
                console.error('error while trying to get token: ',error);
                return 0;
        });
        // window.FirebasePlugin.onTokenRefresh(function(token) {
        //     // save this server-side and use it to push notifications to this device
        //     console.log(token);
        //     alert(token);
        // }, function(error) {
        //     console.error(error);
        // });
        }
    }
    device.init();
    return device;
}