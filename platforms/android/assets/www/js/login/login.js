var login = {
    ajax  : null,
    result: null,
    ip    : null,
    SLLObj: null,
    bckBtn: 0,
    bckHou: null,
    bckMin: null,
    bckSec: null,
    deviceMng: null,
    init: function() {
        document.addEventListener('deviceready', function () {
            login.ip        = new ip();
            login.ip        = login.ip.remoteIpAddress();
            login.ajax      = new ajax();
            login.SLLObj    = new SQLiteLogin();
            login.deviceMng = new deviceManager();
            document.addEventListener("backbutton", function (e) {
                e.preventDefault();
                if(login.bckBtn === 1) {
                    var date = new Date();
                    var hou = date.getHours();
                    var min = date.getMinutes();
                    var sec = date.getSeconds();
                    if(hou === login.bckHou && min === login.bckMin && ((sec-login.bckSec)<2)) {
                        navigator.app.exitApp();
                    } else {
                        login.bckHou = 0;
                        login.bckMin = 0;
                        login.bckSec = 0;
                        login.bckBtn = 0;
                    }
                } else {
                    var date = new Date();
                    login.bckHou = date.getHours();
                    login.bckMin = date.getMinutes();
                    login.bckSec = date.getSeconds();
                    login.bckBtn = 1;
                }
            }, false );
            login.eventListener();
        }, false);
    },
    eventListener: function() {
        $(document).on('click', '#btnLogin', login.loginCheck);
        $(document).on('click', '#signup', function(e) {
            e.preventDefault();
            window.location.href = 'file:///android_asset/www/views/signup/index.html';
        });
        $(document).on('focusout', '#userName', function() {
            if($('#userName').val() === '') {
                $('.userNameError').text('* User name field can not be empty');
            } else {
                $('.userNameError').text('');
            }
        });
        $(document).on('focusout', '#userPassword', function() {
            if ($('#userPassword').val() === '') {
                $('.passwordError').text('* Password field can not be empty');
            } else {
                $('.passwordError').text('');
            }
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="file:///android_asset/www/img/loading.gif">'+
                            '<h5 class="loadingText">Login.....</h5>'+
                        '</div>';
        $('.parent-container').prepend(templete);
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    loginCheck: function(e) {
        e.preventDefault();
        if($('#userName').val() === '') {
            $('.userNameError').text('User name field can not be empty');
            $('.userNameError').css('height','20px;');
            return;
        }
        if ($('#userPassword').val() === '') {
            $('.passwordError').text('Password field can not be empty');
            $('.passwordError').css('height','20px;');
            return;
        }
        login.addLoading();
        var value = {
                user_name: $('#userName').val(),
                password : $('#userPassword').val()
            }
            var url = login.ip+'?r=login-handler/login';
            var result = login.ajax.ajaxCall(url,value);
            console.log(result);
            result.then(function(data){
                console.log(data);
                if(data.status) {
                    var access_token = data.access_token;
                    var data         = data.user_data;
                    var checkStatus  = 0;
                    if($('#rememberMe').is(":checked")) {
                        checkStatus = 1;
                    }
                    var result       = login.SLLObj.pushData(access_token, data, checkStatus);
                    result.done(function(data) {
                        console.log('successfully recorded data and logged in');
                        var idScuccess = login.deviceMng.deviceIdManager();
                        if(idScuccess === 1) {
                            console.log('device id successfully recorded');
                            setTimeout(function() {
                                window.location.href = 'file:///android_asset/www/views/post/post.html';
                            }, 1000);
                        } else {
                            console.log('failed to record device id');
                            setTimeout(function() {
                                window.location.href = 'file:///android_asset/www/views/post/post.html';
                            }, 1000);
                        }
                        
                    // setTimeout(function() {
                        
                    // }, 1000);}
                        // window.plugins.toast.showWithOptions({
                        //     message: "hey there",
                        //     duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself. 
                        //     position: "bottom",
                        //     addPixelsY: -40  // added a negative value to move it up a bit (default 0) 
                        // },
                        // onSuccess, // optional 
                        // onError    // optional 
                        // );
                    }).fail(function(error) {
                        login.removeLoading();
                        console.log(error);
                        $('.divError').text('User name or password is wrong.');
                        $('.divError').css('padding','5px');
                    });
                } else {
                    login.removeLoading();
                    $('.divError').text('User name or password is wrong.');
                    $('.divError').css('padding','5px');
                }
            }).catch(function(error) {
                login.removeLoading();
                console.log(error);
            });
    }
}
login.init();